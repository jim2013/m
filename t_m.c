#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[]){
	void *p;
	p = (int *)malloc(2 * sizeof(int));
	*p = 1;
	*(p + 1) = 2;
	printf("p[1] * p[2] = %d", *p * *(p + 1));
	free(p);

	p = (double *)malloc(2 * sizeof(double));
	*p = 3.14;
	*(p + 1) = 2;
	printf("p[1] * p[2] = %d", *p * *(p + 1));
	free(p);
}
